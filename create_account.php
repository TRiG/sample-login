<?php
require 'application/config.php';

$error = '';
$u = new User;
$email = Core::post('email');
$pass  = Core::post('password');
$name  = Core::post('name');
$conf  = Core::post('confirm');
if ($email && $pass && $name && $conf) {
    if ($pass !== $conf) {
        $error = '
            <p>Password and password confirmation don\'t match.</p>
        ';
    } elseif ($new = $u->create($name, $email, $pass)) {
        Core::go(Core::get('return', '/'));
    } else {
        $error = '
            <p>Unable to create a new account. Perhaps that e-mail address
            is already in use.</p>
        ';
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $error = '<p>Please fill in all fields.</p>';
}

$page_content = array(
    'Title'     => 'Create Account',
    'Content'   => '<form method="post" action="">
                   <label>E-mail: <input type="email" name="email" value="' . HTML::escape($email) . '"></label>
                   <label>User Name: <input type="text" name="name" value="' . HTML::escape($name) . '"></label>
                   <label>Password: <input type="password" name="password"></label>
                   <label>Confirm: <input type="password" name="confirm"></label>
                   <input type="submit" name="go" value="Create Account">
                   </form>' . $error
);

Page::display($page_content, 'xlogin');

