<?php
require 'application/config.php';

Security::require_login();

$page_content = array(
    'Title'     => 'Administration',
    'Content'   => '<p>This page will not work if you are not logged in; instead,
                    it will redirect you to the login page, so if you\'re seeing
                    this, you know that you\'re logged in.</p>
                    <p>There isn\'t actually any site administration here. The site
                    has no editable content. Nor does the site have different levels
                    of users (such as normal accounts, moderator accounts, administrator
                    accounts). However, you can ' . HTML::link('account', 'manage
                    your own account') . '.</p>',
);

Page::display($page_content);

