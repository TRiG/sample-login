<?php
require 'application/config.php';

$page_content = array(
    'Title'     => 'Not found',
    'Content'   => '<p>The page you were looking for was not found.</p>
                    <p>Try <a href="index">the home page</a> instead.</p>'
);

Page::display($page_content);

