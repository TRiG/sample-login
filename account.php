<?php
require 'application/config.php';

Security::require_login(); // You can't administer your account if you're not logged in!

$u = new User;
$name_note = '';
$pass_note = '';
$command = Core::post('cmd');
switch ($command) {
case 'name':
    if ($name = Core::post('name')) {
        $u->update(array('name' => $name));
    } else {
        $name_note = 'You cannot set your username to blank.';
    }
    break;
case 'password':
    $password = Core::post('password');
    $confirm  = Core::post('confirm');
    if ($password !== $confirm) {
        $pass_note = 'The password and confirm fields must match.';
    } elseif ($password) {
        $u->update(array('password' => $password));
        $pass_note = 'Your password has been updated.';
    } else {
        $pass_note = 'You cannot set your password to blank.';
    }
    break;
}

$page_content = array(
    'Title'     => 'Manage Account for ' . HTML::escape($u->name),
    'Content'   => '<form method="post" action="">
                   <label>User Name: <input type="text" name="name" value="' . HTML::escape($u->name) . '"></label>
                   <input type="hidden" name="cmd" value="name">
                   <input type="submit" value="Update User Name">
                   </form>
                   <form method="post" action="">' . $name_note . '
                   <label>Password: <input type="password" name="password"></label>
                   <label>Confirm Password: <input type="password" name="confirm"></label>
                   <input type="hidden" name="cmd" value="password">
                   <input type="submit" value="Update Password">
                   </form>' . $pass_note,
);

Page::display($page_content);

