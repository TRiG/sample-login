<?php
require 'application/config.php';

$page_content = array(
    'Title'     => 'License: CC0',
    'Content'   => '<p>All my own work here is under the
                    <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0
                    License</a>.</p>
                    <iframe src="cc0.txt"></iframe>
                    <p>This Demonstration Login System also includes
                    <a href="https://github.com/ircmaxell/password_compat">Anthony
                    Ferrara\'s compatibility library with PHP 5.5\'s simplified
                    password hashing API</a>. This is under the <a href="http://www.opensource.org/licenses/mit-license.html">MIT
                    License</a>.</p>
                    <iframe src="mit-password-hash.txt"></iframe>
                    ',
);

Page::display($page_content);

