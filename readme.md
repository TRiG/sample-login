Sample Login System
===================

This is a very simple system: you can create an account, log into and out of that
account, and change your username and password. That's it.

However, that is enough to demonstrate

* an interesting .htaccess file,
* a very simple templating system,
* a method for connecting PHP to a MySQL database,
* how to store passwords (using PHP's `password_hash` API),
* the basics of how to use classes,
* how to lay out code into files and folders.

This is intended purely to demonstrate this stuff. I don't pretend it's best practices
(in particular, the Data library is somewhat idiosyncratic), but it works and isn't
terrible.

password_hash
=============

Anthony Ferrara's `password_hash` library is included directly, rather than being
pulled in by Composer. That's because I wanted to keep things simple. This is, after
all, intended as a little desmonstration system, and demonstrating how to use Composer
is somewhat beyond its aims.

Licencing
=========

Anthony Ferrara's `password_hash` library is, as the comments at the top state,
under the [MIT license](http://www.opensource.org/licenses/mit-license.html). All
my own stuff here is [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

