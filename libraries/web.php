<?php

class Web {
    public static function full($path) {
        /*
         * This converts URL in any format into a full URL.
         */
        if (strpos($path, '://')) {
            // If it contains ://, it's already a full URL.
            return $path;
        }
        $url  = isset($_SERVER['HTTPS']) ? 'https' : 'http';
        $url .= '://';
        $url .= $_SERVER['HTTP_HOST'];
        if (substr($path, 0, 1) === '/') {
            // If the URL begins with /, it's relative to the base dir.
            $url .= FILE_PATH . $path;
            return $url;
        } elseif (substr($path, 0, 2) === './') {
            $path = substr($path, 2);
        }
        $reduce = 1;
        while (substr($path, 0, 3) === '../') {
            ++$reduce;
            $path = substr($path, 3);
        }
        $current_url = $url . $_SERVER['REQUEST_URI'];
        $curr = parse_url($current_url);
        $base = $curr['path'];
        do {
            $base = dirname($base . 'index');
            // We add something so that paths with a trailing slash work properly.
            // It really doesn't matter much what text we add.
            var_dump($base);
            --$reduce;
        } while ($reduce);
        if ($base === '/') {
            $base = '';
        }
        $url .= $base;
        $url .= '/' . $path;
        return $url;
    }
}

