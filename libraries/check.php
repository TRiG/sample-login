<?php

class Check {
    public static function integer($n) {
        $i = filter_var($n, FILTER_VALIDATE_INT);
        return is_int($i);
    }
    public static function email($mail) {
        return filter_var($mail, FILTER_VALIDATE_EMAIL) ? true : false;
    }
    public static function web($url) {
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED) ? true : false;
    }
}

