<?php

class Page {
    static public function display($content, $template='default', $return=false) {
        /*
         * A real CMS would have a far more complex templating system than this.
         * This is very quick and easy, and was done in a rush. For example, if
         * a title contains special characters, it's not encoded here: we have to
         * do it by hand.
         */
        $s = file_get_contents(BASE_DIR . '/templates/' . $template . '.tmpl');
        self::set($content);
        foreach ($content as $k => $v) {
            $s = str_replace('[[' . $k . ']]', $v, $s);
        }
        if ($return) {
            return $s;
        } else {
            echo $s;
        }
    }
    static public function set(&$arr) {
        /*
         * The & before the variable means that we receive the variable by reference.
         * That means that changes we make to $arr within this function will appear
         * back in the context in which this function was called.
         *
         * ---------------------------------------------------------------------
         *
         * This function sets system variables into the array of data which is passed
         * to templates.
         */
        $u = new User;
        if ($u->id && $u->name) {
            $arr['system/login'] = 'You are logged in as <strong>' . HTML::link('account', $u->name) . '</strong>. | ' . HTML::link('logout', 'Log Out');
        } elseif ($u->id) {
            // We have a user with no name. This really shouldn't happen. But just
            // in case it does.
            $arr['system/login'] = 'You are logged in as <strong>User ' . $u->id . '</strong>. | ' . HTML::link('account', 'Set Account Details') . ' | ' . HTML::link('logout', 'Log Out');
        } else {
            $arr['system/login'] = HTML::link('login', 'Log In') . ' | ' . HTML::link('create_account', 'Create Account');
        }
    }
}

