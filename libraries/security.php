<?php

class Security {
    public static function require_login() {
        $u = new User;
        if ($u->id) {
            return true;
        } else {
            return Core::go('login?return=' . urlencode($_SERVER['REQUEST_URI']));
        }
    }
}

