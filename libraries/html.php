<?php

class HTML {
    static public function escape($var){
        if (defined('ENT_SUBSTITUTE')) {
            $flags = ENT_QUOTES | ENT_SUBSTITUTE;
        } else {
            $flags = ENT_QUOTES;
        }
        /*
         * ENT_QUOTES
         * $var = 'I said "hello"'
         * This function may be used for escaping strings to be used inside HTML
         * attributes (<img alt="$var">), so we want to make sure quotation marks
         * are converted to HTML entities (<img alt="I said &quot;hello&quot;">).
         * Outside of attributes, this is unnecessary, but does no harm (<h1>I said
         * &quot;hello&quot;</h1>).
         *
         * ENT_SUBSTITUTE
         * Invalid code sequences are replaced with the Unicode Replacement Character
         * U+FFFD (in most fonts, this should show up as a question mark in a diamond).
         */

        return htmlspecialchars($var, $flags, 'UTF-8');
        /*
         * Because this function escapes only 5 characters and passes everything
         * else, it will behave exactly the same in ASCII, ISO-8859-1 (Latin-1),
         * or UTF-8.
         *
         * Characters escaped: & " ' < >
         * Since we're in UTF-8 mode now, we no longer have any need to escape é
         * to &eacute; or € to &euro;, etc.
         */
    }
    static public function link($url, $text='', $extra='') {
        if (!$text) {
            if ($scheme = parse_url($url, PHP_URL_SCHEME)) {
                // full URL
                $strlen = strlen($scheme . '://');
                $text = substr($url, $strlen);
            } else {
                // relative URL
                $text = $url;
            }
        }
        return '<a href="' . self::escape($url) . '"' . $extra . '>' . self::escape($text) . '</a>';
    }
}

