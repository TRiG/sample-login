<?php

/*
 * This code saves you having to write SQL queries by hand. Instead, you can use
 * this library.
 *
 * Database connection details are held in /config/data.php.
 */

class Data {
    private
        $link,
        $server,
        $username,
        $password,
        $database;
    public
        $queries = 0,
        $sql,
        $sqls = array();
    public static function get($name='data') {
        static $instances = array();
        if (isset($instances[$name])) {
            return $instances[$name];
        }
        return $instances[$name] = new self($name);
    }
    private function __construct($name) {
        $a = Core::config($name);
        $this->key      = 'id';
        $this->server   = Core::val($a, 'server',   $this->server,   true);
        $this->username = Core::val($a, 'username', $this->username, true);
        $this->password = Core::val($a, 'password', $this->password, true);
        $this->database = Core::val($a, 'database', $this->database, true);
        $this->config   = $name; // for tracking purposes only
    }
    private function open() {
        if ($this->link = mysqli_connect($this->server, $this->username, $this->password)) {
            return mysqli_select_db($this->link, $this->database);
        } else {
            return false;
        }
    }
    public function escape($str) {
        if (!$this->link) {
            $this->open();
        }
        return mysqli_real_escape_string($this->link, $str);
    }
    public function run($sql) {
        $this->queries++;
        $this->sqls[] = array('config' => $this->config, 'sql' => $sql);
        $this->sql = $sql;
        if (!$this->link) {
            $this->open();
        }
        $this->result = mysqli_query($this->link, $sql);
        if ($this->result === false) {
            /*
             * Return values:
             *  False on failure.
             *  A mysqli_result object for successful SELECT, SHOW, DESCRIBE or EXPLAIN queries.
             *  True for other successful queries.
            */
            return false;
        }
        $this->affected = mysqli_affected_rows($this->link);
        if ($this->affected === -1) {
            /*
             * The number of rows affected by the query. Or, if the query was a
             * SELECT, and didn't change anything in the database, the number of
             * rows returned by the query. This may be zero. If it's minus one,
             * there's an error. That would probably have been caught by the last
             * check, but we'll check for it here too.
             */
            return false;
        }
        return true;
    }
    public function row($table, $where, $vals=0, $extra='') {
        $a = $this->select($table, $vals, $where, $extra);
        return Core::sval($a, 0, array());
    }
    public function value($table, $where, $val, $extra='', $default='', $return=1) {
        $a = $this->select($table, $val, $where, $extra);
        if ($return && ($a[0][$val] === '0')) {
            return 0;
        }
        return $a[0][$val] ? $a[0][$val] : $default;
    }
    public function listing($table, $val, $where='', $extra='') {
        $a = $this->select($table, array($this->key, $val), $where, $extra);
        $rows = array();
        foreach ($a as $v) {
            $rows[$v[$this->key]] = $v[$val];
        }
        return $rows;
    }
    public function select($table, $vals=0, $where='', $extra='') {
        $q = '';
        if (is_array($vals)) {
            foreach ($vals as $v) {
                if ($q) $q .= ', ';
                $q .= '`' . $this->escape($v) . '`';
            }
        } elseif (!$vals) {
            $q = '*';
        } else {
            $q = $vals;
        }
        if (is_array($where)) {
            $p = '';
            foreach ($where as $k => $v) {
                if ($p) {
                    $p .= ' AND';
                }
                $p .= ' (`' . $this->escape($k) . '`="' . $this->escape($v) . '")';
            }
            $where = $p;
        } elseif (Check::integer($where)) {
            $where = '`' . $this->key . '`=' . $where;
        }
        $sql = '
            SELECT
                    ' . $q . '
            FROM
                    `' . $table . '`
            ' . ($where ? ' WHERE ' . $where : '') . $extra;
        return $this->load($sql);
    }
    public function load($sql) {
        // For custom selects. Custom updates, inserts, and replaces can use this->run().
        if (!$this->run($sql)) {
            return false;
        }
        $this->rows = mysqli_num_rows($this->result);
        $rows = array();
        // so if no result returns empty array, not null. Thus it should be safe
        // to use the result in a foreach loop.
        while ($row = mysqli_fetch_assoc($this->result)) {
            $rows[]= $row;
        }
        return $rows;
    }
    public function insert($table, $arr, $ignore=false, $nullable=false) {
        $fields = '';
        $values = '';
        foreach ($arr as $k => $v) {
            if ($fields) {
                $fields .= ', ';
            }
            if ($values) {
                $values .= ', ';
            }
            $fields .= '`' . $this->escape($k) . '`';
            if (is_null($v) && $nullable) {
                $values .= 'NULL';
            } else {
                $values .= '"' . $this->escape($v) . '"';
            }
        }
        $sql = '
            INSERT' . ($ignore ? ' IGNORE' : '') . ' INTO
                    `' . $table . '`
                        (' . $fields . ')
            VALUES
                    (' . $values . ')
        ';
        if ($this->run($sql)) {
            return $this->insert_id = mysqli_insert_id($this->link);
        }
        return false;
    }
    public function update($table, $arr, $where='') {
        $u = '';
        foreach ($arr as $k => $v) {
            if ($u) $u .= ', ';
            $u .= '`' . $this->escape($k) . '`="' . $this->escape($v) . '"';
        }
        $sql = 'UPDATE `' . $table . '` SET ' . $u;
        if (is_array($where)) {
            $p = '';
            foreach ($where as $k => $v) {
                if ($p) $p .= ' AND';
                $p .= ' (`' . $this->escape($k) . '`="' . $this->escape($v) . '")';
            }
            $where = $p;
        } elseif (Check::integer($where)) {
            $where = '`' . $this->key . '`=' . $where;
        }
        $sql .= ($where ? ' WHERE ' . $where : '');
        return $this->run($sql);
    }
}

