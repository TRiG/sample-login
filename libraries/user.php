<?php

class User {
    public function __construct() {
        if (!defined('PASSWORD_DEFAULT')) {
            // We must be running on an old version of PHP which doesn't include
            // password_hash() and related functions. So we include a script which
            // adds them.
            require_once dirname(dirname(__FILE__)) . '/helpers/password.php';
        }
        $this->id = Core::sval($_SESSION, 'user_id');
        if ($this->id) {
            $this->name = Core::val($_SESSION, 'user_name');
            return $this->id;
        }
    }
    public function login($email, $password) {
        $d = Data::get();
        $u = $d->row(
                'user',
                array(
                    'email' => $email
                ),
                array(
                    'id',
                    'name',
                    'phash'
                )
        );
        if (!$u) {
            return false;
        } elseif ($this->verify($u, $password)) {
            return $this->start($u);
        } else {
            return false;
        }
    }
    public function create($name, $email, $password) {
        $d = Data::get();
        $new = $d->insert(
                'user',
                array(
                    'name'  => $name,
                    'email' => $email,
                    'phash' => $this->hash($password),
                )
        );
        if ($new) {
            return $this->start(
                    array(
                        'name'  => $name,
                        'id'    => $new
                    )
            );
        } else {
            return false;
        }
    }
    public function logout() {
        return session_destroy();
    }
    public function update($arr, $id=null) {
        if (!$id) {
            // If no ID is specified, assume the ID of the current user.
            $id = $this->id;
        }
        if (!$id) {
            // The current user is not logged in.
            return false;
        }
        $a = array();
        if ($name = Core::val($arr, 'name')) {
            $a['name'] = $name;
        }
        if ($password = Core::val($arr, 'password')) {
            $a['phash'] = $this->hash($password);
        }
        if ($a) {
            $d = Data::get();
            $d->update('user', $a, $id);
            if ($name && ($id === $this->id)) {
                // We should update the details of the current user.
                $a['id'] = $id;
                return $this->start($a);
            } else {
                return $id;
            }
        } else {
            return false;
        }
    }
    private function verify($user, $password) {
        /*
         * This function receives user details and a password. It must check whether
         * the details match.
         *
         * The password_hash() function will give a different result each time it
         * hashes a password. That means that we can't just hash the password and
         * then request from the database to return a row where the password and
         * e-mail address match. Instead, we just ask it to return a row where the
         * e-mail address matches, and then checks whether the provided password
         * also matches.
         */
         return password_verify($password, $user['phash']);
         // This checks the provided password against the stored hash. It returns
         // true or false.
    }
    private function start($u) {
        $_SESSION['user_id'] = $u['id'];
        $_SESSION['user_name'] = $u['name'];
        $this->id = $u['id'];
        $this->name = $u['name'];
        return $this->id;
    }
    private function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}

