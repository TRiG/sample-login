<?php

class Core {

    static $config = array();
    
    static public function setup() {
        session_start();
        define('BASE_DIR', dirname(dirname(__FILE__)));
        define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
        $subfolder = (string)substr(BASE_DIR, strlen(DOCUMENT_ROOT));
        define('FILE_PATH', $subfolder);
        ini_set('display_errors', 1);
        error_reporting(-1);
        set_error_handler(array('Core', 'on_error'));
        set_exception_handler(array('Core', 'on_exception'));
        spl_autoload_register(array('Core', 'autoload'));
    }
    static public function config($name) {
        if (array_key_exists($name, self::$config)) {
            return self::$config[$name];
        }
        return self::$config[$name] = require BASE_DIR . '/config/' . $name . '.php';
    }
    static public function autoload($class_name) {
        $file_name = str_replace('_', '/', $class_name) . '.php';
        $file_name = strtolower($file_name);
        $file_path = BASE_DIR . '/libraries/' . $file_name;
        if (file_exists($file_path)) {
            return require $file_path;
        } else {
            trigger_error('Unknown class: ' . $class_name);
        }
    }
    public static function val($arr, $name, $default='') {
        if (isset($arr[$name]) && $arr[$name] !== '' && $arr[$name] !== null) {
            return $arr[$name];
        } else {
            return $default;
        }
    }
    public static function sval($arr, $name, $default=null) {
        // strict val: return value if it's set in the array at all.
        return isset($arr[$name]) ? $arr[$name] : $default;
    }
    static public function get($name, $default=null) {
        return self::sval($_GET, $name, $default);
    }
    static public function post($name, $default=null) {
        return self::sval($_POST, $name, $default);
    }
    static public function go($url, $status=302) {
        $url = Web::full($url);
        // Most browsers will follow a relative URL redirect, but the specs say
        // that, properly speaking, a full URL is required.
        $type = ($status == 301 ? 'permanently' : 'temporarily');
        header('Content-Type: text/plain;charset=utf-8');
        header('Location: ' . $url, true, $status);
        echo 'Document moved ' . $type . PHP_EOL . 'Location: ' . $url;
        // http://stackoverflow.com/a/9912573/209139
        exit;
    }
    public static function on_exception($e) {
        self::on_error($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
    }
    static public function on_error($code, $str, $file='', $line=0, $context=array()) {
        $s  = '<b>' . $str . '</b><br>' . PHP_EOL;
        $s .= '<hr>' . $file . ' : ' . $line . '<br>' . PHP_EOL . '<hr><br>' . PHP_EOL . PHP_EOL;
        $s .= '<pre>' . print_r($context, true) . '</pre>' . PHP_EOL;
        echo $s;
        $e = new Email;
        $e->to   = 'timothy@green.name';
        $e->from = $e->to;
        $e->subj = 'Bug: ' . $file;
        $e->text = $s;
        $e->send();
        exit;
    }
}

