<?php

class Email {

    public
        $from,
        $to,
        $subj,
        $text,
        $mime = 'text/html';
        
    public function send() {
        $h[] = 'From: ' . $this->from;
        $h[] = 'Content-Type: ' . $this->mime;
        $h[] = 'X-Mailer: TRG Engine v.1';
        /*
         * PHP_EOL is whatever the line ending is on the platform we're currently
         * running on. On Linux, that's \n. On Old Mac, it's \r. On Windows and
         * New Mac, it's \r\n. Mail headers *must* be \r\n, so we can't rely on
         * PHP_EOL, so we write the separator directly.
         */
        return mail($this->to, $this->subj, $this->text, implode("\r\n", $h));
    }
}

