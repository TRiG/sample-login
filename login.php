<?php
require 'application/config.php';

$error = '';
$u = new User;
if ($u->id) {
    // The user is already logged in.
    Core::go(Core::get('return', '/'));
} elseif ($_POST) {
    $email = Core::post('email');
    $pass  = Core::post('password');
    $id = $u->login($email, $pass);
    if ($id) {
        Core::go(Core::get('return', '/'));
    } else {
        $error = '<p>Could not log you in. Please try again.</p>';
    }
}

// An input without a name attribute is not submitted to the server as part of the
// request.

$page_content = array(
    'Title'     => 'Log In',
    'Content'   => '<form method="post" action="">
                   <label>E-mail: <input type="email" name="email"></label>
                   <label>Password: <input type="password" name="password"></label>
                   <input type="submit" value="Log In">
                   </form>' . $error . '<p>If you do not already have an account,
                   you can <a href="create_account">create one</a>.</p>'
);

Page::display($page_content, 'xlogin');

