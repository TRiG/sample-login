<?php
require 'application/config.php';

$page_content = array(
    'Title'     => 'Welcome!',
    'Content'   => '<p>This is a test website to generate login. This page will
                    show at the top whether or not you are logged in.</p>
                    <p>On the other hand, <a href="admin">the admin page</a> will
                    not show at all if you are not logged in.</p>'
);

Page::display($page_content);

