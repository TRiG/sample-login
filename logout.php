<?php
require 'application/config.php';

$error = '';
$u = new User;
if (!$u->id) {
    // The user not logged in.
    Core::go(Core::get('return', '/'));
} elseif ($_POST) {
    /*
     * We should log a user out (or in) only on a POST request. This is a guard
     * against XSS attacks.
     *
     * Basically, someone on some other website could include something like
     * <img src="http://example.com/logout">. Anyone who visited that page would
     * then also load this page. Since this page doesn't return a valid image, the
     * result would be ignored. But if that visit had a side-effect, like logging
     * the user out, that would be a bad thing. (If it logged the user in, or transferred
     * money from one bank account to another, that would, of course, be worse.)
     * Logging a user out is perhaps the mildest form of XSS attack, but we still
     * want to guard against it. The basic principle is that GET requests should
     * only result in displaying data. To actually *do* anything, you need a POST
     * request.
     *
     * XSS is actually quite a complicated subject, but that's a good start.
     *
     * (It stands for cross-site scripting, by the way. Clearly, the acronym CSS
     * was already taken for cascading style sheets.)
     */
    $cmd = Core::post('cmd');
    $u->logout();
    Core::go(Core::get('return', '/'));
}

$page_content = array(
    'Title'     => 'Log Out',
    'Content'   => '<form method="post" action="">
                   <input type="submit" value="Log Out" name="cmd">
                   </form>'
);

Page::display($page_content, 'xlogin');

